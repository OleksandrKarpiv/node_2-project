const {handlerResponse} = require('../response.handlers');
const {noteService} = require('../services/note.service');

const notesController = {
  postNote: async (req, res) => {
    try {
      const {username} = req.VerifiedUser;
      const {text} = req.body;

      const userDataFromDB = await noteService.createPost(username, text);
      if (!userDataFromDB) {
        throw new Error('Error happened');
      }
      res.send({message: 'Success'});
    } catch (e) {
      handlerResponse.errorHandlerServer(res);
    }
  },

  getNote: async (req, res) => {
    try {
      const {username} = req.VerifiedUser;
      const {offset, limit} = req.body;
      const notes = await noteService.getNotes(username, +offset, +limit);
      const user = await noteService.getAllNotes(username);
      res.send({notes: notes[0].notes, offset: +offset, limit: +limit,
        count: Math.round((user[0].notes.length / limit))});
    } catch (e) {
      handlerResponse.errorHandlerServer(res);
    }
  },

  getNoteById: async (req, res) => {
    try {
      const {id} = req.params;
      const {username} = req.body;
      const post = await noteService.getPostById(username, id);
      res.send({note: post});
    } catch (e) {
      handlerResponse.errorHandlerServer(res);
    }
  },

  updateNoteById: async (req, res) => {
    try {
      const {id} = req.params;
      const {text} = req.body;
      const {username} = req.VerifiedUser;
      await noteService.updateNoteById(username, id, text);
      handlerResponse.successHandlerServer(res);
    } catch (e) {
      handlerResponse.errorHandlerServer(res);
    }
  },
  deleteNoteById: async (req, res) => {
    try {
      const {id} = req.params;
      const {username} = req.VerifiedUser;
      await noteService.deleteNoteById(username, id);
      handlerResponse.successHandlerServer(res);
    } catch (e) {
      handlerResponse.errorHandlerServer(res);
    }
  },

  checkNoteById: async (req, res) => {
    try {
      const {id} = req.params;
      const {username} = req.VerifiedUser;
      await noteService.checkNoteById(username, id);
      handlerResponse.successHandlerServer(res);
    } catch (e) {
      handlerResponse.errorHandlerServer(res);
    }
  },
};

module.exports = {notesController,
};
