const {_hashPassword, comparePassword} = require('../helpers/bycrypt.helper');
const {handlerResponse} = require('../response.handlers');
const {authService} = require('../services/auth.service');
const {userService} = require('../services/user.service');
const {Token} = require('../tokens/token');

const authController = {
  registerUser: async (req, res) => {
    try {
      const {username, password} = req.body;
      if (!username || !password) {
        return handlerResponse.errorHandlerClient(res);
      }
      const user = await authService.checkIfUserExists(username);
      if (user) return handlerResponse.errorHandlerServer(res);

      const hashPassword = await _hashPassword(password);

      await userService.createUser({username, password: hashPassword});

      return handlerResponse.successHandlerServer(res);
    } catch (e) {
      return handlerResponse.errorHandlerServer(res);
    }
  },

  loginUser: async (req, res) => {
    try {
      const {username, password} = req.body;
      if (!username || !password) {
        return handlerResponse.errorHandlerClient(res);
      }

      const user = await authService.checkIfUserExists(username);
      if (!user) return handlerResponse.errorHandlerServer(res);

      const isPasswordRight = comparePassword(password, user.password);
      if (!isPasswordRight) return handlerResponse.errorHandlerServer(res);

      const token = Token.generateToken(user);
      res.status(200).send({message: 'Success', jwt_token: token});
    } catch (e) {
      console.log(e.message);
      res.status(400).send('string');
    }
  },
};

module.exports = {
  authController,
};
