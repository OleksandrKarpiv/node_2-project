const {handlerResponse} = require('../response.handlers');
const {userService} = require('../services/user.service');

const usersController = {
  getInfoUser: async (req, res) => {
    try {
      const {username} = req.VerifiedUser;

      const userDataFromDB = await userService.findUserByName(username);
      if (!userDataFromDB) {
        throw new Error('User not found');
      }
      res.send({user: {
        username: userDataFromDB.username,
        _id: userDataFromDB._id,
        createdDate: userDataFromDB.createdDate,
      }});
    } catch (e) {
      handlerResponse.errorHandlerServer(res);
    }
  },
};

module.exports = {
  usersController,
};
