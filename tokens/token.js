const {config} = require('../config');
const jwt = require('jsonwebtoken');

const Token = {
  generateToken: ({username, password}) => {
    const jwtToken = jwt.sign({username, password},
        config.SECRET_ACCESS_KEY, {expiresIn: '2d'});
    return jwtToken;
  },
};

module.exports = {
  Token,
};
