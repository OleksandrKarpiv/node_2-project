const {json} = require('express');
const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const mongoose = require('mongoose');

const {routes} = require('./routes/api.routers');

const app = express();
morgan.token('body', (req) => {
  return JSON.stringify(req.body);
});

mongoose.connect('mongodb+srv://sashaKarp:sashaKarp@cluster0.pximg.mongodb.net/UsersDB?retryWrites=true&w=majority',
    {useNewUrlParser: true}).then(() => console.log('Mongo connected'))
    .catch((err) => console.log(err));

app.use(morgan(':method :url :body'));

app.use(cors());
const port = 8080;


app.use(json());
app.use('/api/', routes);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
