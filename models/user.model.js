const {default: mongoose} = require('mongoose');

const noteSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'users',
    required: true,
  },
  completed: {
    type: Boolean,
    required: true,
  },
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    required: true,
    default: Date.now,
  },
});

const UsersSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true,
  },

  createdDate: {
    type: Date,
    default: Date.now,
  },

  password: {
    type: String,
    required: true,
  },

  notes: {
    type: [noteSchema],
  },
});

const Users = mongoose.model('Users', UsersSchema);
module.exports = {
  Users,
};
