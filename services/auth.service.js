const {Users} = require('../models/user.model');

const authService = {
  checkIfUserExists: async (username) => {
    const doesExist = await Users.findOne({username});
    console.log('user', doesExist);
    return doesExist;
  },

};

module.exports = {
  authService,
};
