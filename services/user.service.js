const {Users} = require('../models/user.model');

const userService = {
  createUser: async ({username, password}) => {
    try {
      return await Users.create({
        username: username,
        password: password,
      });
    } catch (e) {
      console.log(e);
    }
  },

  findUserByName: async (username) => {
    return await Users.findOne({
      username,
    });
  },
};

module.exports = {
  userService,
};
