const {default: mongoose} = require('mongoose');
const {Users} = require('../models/user.model');

const noteService = {
  createPost: async (username, mess) => {
    const userFromDb = await Users.findOne({username: username});
    userFromDb.notes.push({
      completed: false,
      text: mess,
      userId: userFromDb._id,
    });
    return await userFromDb.save();
  },

  getNotes: async (username, offset, limit) => {
    return await Users.find({username: username},
        {notes: {$slice: [offset, limit]}});
  },

  getAllNotes: async (username) => {
    return await Users.find({username: username});
  },

  getPostById: async (username, id) => {
    const userFromDb = await Users.findOne({username: username});
    return await userFromDb.notes.find(((post) =>
      JSON.stringify(post._id) === JSON.stringify(id)));
  },

  updateNoteById: async (username, id, text) => {
    const userFromDb = await Users.findOne({username: username});
    userFromDb.notes.forEach((note) => {
      if (JSON.stringify(note._id) === JSON.stringify(id)) {
        note.text = text;
      }
    });

    return await userFromDb.save();
  },

  checkNoteById: async (username, id) => {
    const userFromDb = await Users.findOne({username: username});
    userFromDb.notes.forEach((note) => {
      if (JSON.stringify(note._id) === JSON.stringify(id)) {
        note.completed = !note.completed;
      }
    });

    return await userFromDb.save();
  },

  deleteNoteById: async (username, id) => {
    const userFromDb = await Users.findOne({username: username});
    userFromDb.notes.remove(mongoose.Types.ObjectId(id));
    userFromDb.save();
  },
};

module.exports = {
  noteService,
};
