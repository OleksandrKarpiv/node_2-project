const jwt = require('jsonwebtoken');
const {config} = require('../config');
const {handlerResponse} = require('../response.handlers');


const authMiddlewares = {
  checkValidToken: (req, res, next) => {
    console.log(req.header('Authorization'));
    try {
      const token = req.header('Authorization');
      const tokenData = jwt.verify(token, config.SECRET_ACCESS_KEY);
      if (token) {
        req.VerifiedUser = tokenData;
        next();
      }
    } catch (e) {
      console.log(e.message);
      handlerResponse.errorHandlerClient(res);
    }
  },
};

module.exports = {
  authMiddlewares,
};
