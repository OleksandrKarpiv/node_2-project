const bcrypt = require('bcrypt');

const _hashPassword = async (password) => {
  return bcrypt.hash(password, 10);
};

const comparePassword = async (credPass, DbHash) => {
  return bcrypt.compare(credPass, DbHash);
};


module.exports = {
  _hashPassword,
  comparePassword,
};
