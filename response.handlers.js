const handlerResponse = {
  errorHandlerClient: (res) => {
    return res.status(400).send(JSON.stringify({message: 'string'}));
  },

  errorHandlerServer: (res) => {
    return res.status(500).send(JSON.stringify({message: 'string'}));
  },

  successHandlerServer: (res) => {
    return res.status(200).send(JSON.stringify({message: 'Success'}));
  },
};

module.exports = {
  handlerResponse,
};
