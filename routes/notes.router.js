const {Router} = require('express');
const {notesController} = require('../controllers/notes.controller');
const {authMiddlewares} = require('../middlewares/auth.middlewares');

const notesRouter = Router();

notesRouter.get('/notes', authMiddlewares.checkValidToken,
    notesController.getNote);

notesRouter.post('/notes', authMiddlewares.checkValidToken,
    notesController.postNote);

notesRouter.get('/notes/:id', authMiddlewares.checkValidToken,
    notesController.getNoteById);

notesRouter.put('/notes/:id', authMiddlewares.checkValidToken,
    notesController.updateNoteById);

notesRouter.patch('/notes/:id', authMiddlewares.checkValidToken,
    notesController.checkNoteById);

notesRouter.delete('/notes/:id', authMiddlewares.checkValidToken,
    notesController.deleteNoteById);

module.exports = {notesRouter};
