const {Router} = require('express');
const {authRouter} = require('./auth.router');
const {notesRouter} = require('./notes.router');
const {usersRouter} = require('./users.router');

const routes = Router();

routes.use('/auth', authRouter);
routes.use('/users', usersRouter);
routes.use('/', notesRouter);

module.exports = {routes};
