const {Router} = require('express');
const {usersController} = require('../controllers/users.controller');
const {authMiddlewares} = require('../middlewares/auth.middlewares');

const usersRouter = Router();

usersRouter.get('/me', authMiddlewares.checkValidToken,
    usersController.getInfoUser);

module.exports = {usersRouter};
