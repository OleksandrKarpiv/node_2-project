const config = {
  SECRET_ACCESS_KEY: process.env.SECRET_KEY || 'YOURSECRETKEYGOESHERE',
};

module.exports = {
  config,
};
